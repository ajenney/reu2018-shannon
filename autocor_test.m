% Trying out lagged correlation stuff

% Create red noise time series of length 10000, with a lag-1 autocorrelation
% of 0.9. Name it x
x = rednoise(0.9, 1, 10000);

% Compute the cross-correlation of the vector. Normalize the result so
% values range from -1 to 1
[r, lags] = xcorr(x,'coeff');

% Plot
figure
plot(lags, r)
xlim([-500 500])