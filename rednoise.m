function [ redt ] = rednoise(ac,lag,N)
%Generate red noise time series of length N, with lag(x)
%autocorrelation of ac. 
%     x(t) = ax(t-dt) + br
%       a: the autocovariance at lag dt
%       b: sqrt(1-a^2)
%       r: random variable drawn from standard normal distribution

    a = ac;
    b = sqrt(1 - a.^2);
    
    redt = NaN([N 1]);
    
    
    % Initialize red noise timeseries with random data, depending on lag
    redt(1:lag) = randsample(randn([1000 1]), 1);
    
    % Create remainder of red noise timeseries
    for i = lag:N-1
        redt(i+1) = (a(1) * redt(i-lag+1)) + ...
            (b * randsample(randn([1000 1]),1));
    end


end

